Name:           perl-Pod-Markdown
Version:        3.400
Release:        2
Summary:        Convert POD text to Markdown
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Pod-Markdown
Source0:        https://cpan.metacpan.org/authors/id/R/RW/RWSTAUNER/Pod-Markdown-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  findutils make perl-interpreter perl-generators perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(strict) perl(warnings) perl(Encode) perl(parent) perl(Pod::Simple) >= 3.27
BuildRequires:  perl(Pod::Simple::Methody) perl(Exporter) perl(File::Spec)
BuildRequires:  perl(File::Spec::Functions) perl(File::Temp) perl(IO::Handle) perl(IPC::Open3)
BuildRequires:  perl(lib) perl(Symbol) perl(Test::More) >= 0.88 perl(utf8) perl(version)
BuildRequires:  perl-Test-Differences

%description
This program uses Pod::Markdown to convert POD into Markdown sources.

%package_help

%prep
%autosetup -n Pod-Markdown-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%license LICENSE
%{perl_vendorlib}/*
%{_bindir}/*

%files help
%doc Changes README
%{_mandir}/man[13]/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 3.400-2
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Dec 03 2024 xu_ping <707078654@qq.com> - 3.400-1
- Upgrade version to 3.400
  * Add CLI options for local-module, man, and perldoc url prefixes.
    Thanks josch!

* Fri Jul 19 2024 yaoxin <yao_xin001@hoperun.com> - 3.300-2
- License compliance rectification

* Tue Jun 14 2022 SimpleUpdate Robot <tc@openeuler.org> - 3.300-1
- Upgrade to version 3.300

* Mon Nov 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.101-2
- Package init
